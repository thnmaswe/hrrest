package de.gsohs.efi.maswe.webappember.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Entity implementation class for Entity: Employee
 *
 */
@Entity
@Table(name="EMPLOYEES")
@XmlRootElement(name="employee")
public class Employee implements Serializable {

	   
	@Id
	@Column(name="EMPLOYEE_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="EMPLOYEE_ID_GEN")
	@SequenceGenerator(name="EMPLOYEE_ID_GEN",sequenceName="EMPLOYEES_SEQ")
	@XmlID
	private Integer id;
	@Column(name="FIRST_NAME")
	private String firstName;
	@Column(name="LAST_NAME")
	private String lastName;
	private String email;
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;
	@Column(name="HIRE_DATE")
	@Temporal(TemporalType.DATE)
	private Date hireDate;
	@Column(name="JOB_ID")
	private String jobId;
	private Float salary;
	@Column(name="COMMISSION_PCT")
	private Float commisionPct;
	@ManyToOne
	@JoinColumn(name="MANAGER_ID",referencedColumnName="EMPLOYEE_ID")
	@XmlIDREF
	private Employee manager;
	@ManyToOne
	@JoinColumn(name="DEPARTMENT_ID")
	@XmlIDREF
	private Department department;
	
	@XmlTransient
	private static final long serialVersionUID = 1L;

	public Employee() {
		super();
	}   
	public Integer getEmployeeId() {
		return this.id;
	}

	public void setEmployeeId(Integer employeeId) {
		this.id = employeeId;
	}   
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}   
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}   
	public Date getHireDate() {
		return this.hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}   
	public String getJobId() {
		return this.jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}   
	public Float getSalary() {
		return this.salary;
	}

	public void setSalary(Float salary) {
		this.salary = salary;
	}   
	public Float getCommisionPct() {
		return this.commisionPct;
	}

	public void setCommisionPct(Float commisionPct) {
		this.commisionPct = commisionPct;
	}   
	public Employee getManager() {
		return this.manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}   
	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
   
}
