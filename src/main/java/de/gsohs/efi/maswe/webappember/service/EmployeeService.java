package de.gsohs.efi.maswe.webappember.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import de.gsohs.efi.maswe.webappember.model.Employee;


/**
 * Stateless Session Bean acting as Service interface for retrieving Employee
 * JPA Entity instances and passing it to the calling objects
 */
@Stateless(name = "EmployeeService")
public class EmployeeService
        implements EmployeeServiceLocal
{

	/**
     * injects an instance of a Container managed JPA EntityManager
     * the mentioned persistence unit is located at src/main/resources/META-INF/persistence.xml
     */
    @PersistenceContext(unitName="hrPU")
    private EntityManager em;

    /**
     * default empty constructor
     */
    public EmployeeService() {
    }

    /**
     * retrieves employees based on two filter criteria (department and lastName)
     * uses the JPA 2 Criteria API 
     * @see http://docs.oracle.com/javaee/6/tutorial/doc/gjitv.html
     */
	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> findByDepartmentAndLastName(Integer deptId, String sn) {
		// first ask the entityManager for a criteria Builder instance
		CriteriaBuilder cb = em.getCriteriaBuilder();
		// criteria builder can construct typed criteriaQuery instances
		CriteriaQuery<Employee> q = cb.createQuery(Employee.class);
		// the root interface reflects the FROM clause in SQL.
		// in our example only employee instances will be fetched
		Root<Employee> c = q.from(Employee.class);
		if(deptId != null) {
			// restrictions are added to the query via typed expression objects
			// here an equal expression checks for equality of given deptId and
			// the department relation in the employee entity (query root)
			q.where(cb.equal(c.get("department").get("id"), deptId));
		}
		if(sn != null && !sn.trim().equals("")) {
			// example of a like expression
			q.where(cb.like(c.<String>get("lastName"), sn+"%"));
		}
		// the criteria query has to be compiled by the entityManager.
		// the result is given back as generic Query instance
		Query qres = em.createQuery(q);
		// query will be executed against db and its result given back to caller
		return qres.getResultList();
	}

}
