package de.gsohs.efi.maswe.webappember.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import de.gsohs.efi.maswe.webappember.model.Department;
import de.gsohs.efi.maswe.webappember.model.Employee;

/**
 * Stateless Session Bean implementation class
 * This Bean is a Fassade to the clients acting as RESTful
 * service. It uses JAX-RS to expose data via JSON and interactions
 * based on HTTP. The JSON processing is done with JavaEE's JSONP spec
 * 
 * @see https://docs.oracle.com/javaee/7/tutorial/jaxrs.htm
 * @see https://docs.oracle.com/javaee/7/tutorial/jsonp002.htm
 */

// JAX-RS annotation. this path segment applies to all methods in this class
@Path("/v1")
// JAX-RS annotation. all methods share this "Content-Type" annotated here.
@Produces(value={"application/vnd.api+json"})
// Stateless Session Bean
@Stateless
public class RestService {
	
	/**
	 * EJB injection by java type (interface). 
	 * Container injects an instance automatically at runtime
	 */
	@EJB
	DepartmentServiceLocal deptDao;
	
	/**
	 * EJB injection by java type (interface). 
	 * Container injects an instance automatically at runtime
	 */
	@EJB
	EmployeeServiceLocal empDao;

	/**
	 * method exposed via http for Rest-CLients. retrieves
	 * all departments in database, convert this into a
	 * JSON object structure and returning it as response
	 * to the RESTful client
	 * 
	 * @return a JSON Object representing the data
	 */
	// this method is bound to HTTP GET method
	@GET
	// this method replies to URL "[host]/[context]/v1/departments"
	@Path("/departments")
	public JsonObject getDepartments() {
		List<Department> depts = deptDao.findAll();
		JsonArrayBuilder da = Json.createArrayBuilder();
		for (Department dept : depts) {
			da.add(Json.createObjectBuilder()
					.add("type", "department")
					.add("id", dept.getDepartmentId().toString())
					.add("attributes", Json.createObjectBuilder()
							.add("name", dept.getDepartmentName())));
		}
		return Json.createObjectBuilder()
				.add("data", da).build();
	}
	
	/**
	 * method exposed via http for Rest-CLients. retrieves
	 * a department by id (ID given as path segment),
	 * convert this into a JSON object structure and returning it 
	 * as response to the RESTful client
	 * 
	 * @return a JSON Object representing the data
	 */
	// this method is bound to HTTP GET method
	@GET
	// example for variable path segments. the department id
	// is marked as variable that can be evaluated as method parameter
	@Path("/departments/{id}")
	public JsonObject getDepartmentById(
			// this is the variable path segment that can be used in the method
			@PathParam("id") Integer id) {
		Department dept =  deptDao.findOne(id);
		JsonObject ret = Json.createObjectBuilder()
				.add("name", dept.getDepartmentName())
				.add("location", dept.getLocation())
				.build();
		return ret;
	}
	
	
	/**
	 * method exposed via http for Rest-CLients. retrieves
	 * a filtered list of employees. the filter is applied
	 * when special query parameters are appended to the URL
	 * [baseURL]/v1/employees[?department=60][&lastName=K]
	 * 
	 * @return a JSON Object representing the data
	 */
	// this method is bound to HTTP GET method
	@GET
	// this method replies to URL "[host]/[context]/v1/departments"
	@Path("/employees")
	public JsonObject getEmployeesByDepartment(
			// query parameters (appended to the URL) can be evaluated
			// with QueryParam annotated method parameters
			@QueryParam(value="department") Integer deptId, 
			@QueryParam(value="surname") String sn) {
		List<Employee> emps = empDao.findByDepartmentAndLastName(deptId, sn);
		JsonArrayBuilder da = Json.createArrayBuilder();
		for(Employee emp : emps) {
			JsonObjectBuilder empob = Json.createObjectBuilder()
					.add("type", "employee")
					.add("id", emp.getEmployeeId().toString())
					.add("attributes", Json.createObjectBuilder()
							.add("surname", emp.getLastName())
							.add("givenname", emp.getFirstName()));
			if(emp.getDepartment() != null) {
				empob.add("relationships", Json.createObjectBuilder()
						.add("department", Json.createObjectBuilder()
								.add("data", Json.createObjectBuilder()
										.add("id", emp.getDepartment().getDepartmentId().toString())
										.add("type", "department")))
								.add("links",Json.createObjectBuilder()
										.add("self", UriBuilder.fromMethod(RestService.class, "getDepartmentById").build(emp.getDepartment().getDepartmentId()).toString())
								)
				);
			}
			da.add(empob);
		}
		return Json.createObjectBuilder().add("data", da).build();
	}

}
