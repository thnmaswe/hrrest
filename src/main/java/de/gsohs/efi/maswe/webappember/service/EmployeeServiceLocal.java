package de.gsohs.efi.maswe.webappember.service;

import java.util.List;

import javax.ejb.Local;

import de.gsohs.efi.maswe.webappember.model.Employee;


/**
 * service interface marked as local, meaning that this EJB will not be
 * shared across multiple Java Virtual Machines, Appservers or RichClient interactions 
 */
@Local
public interface EmployeeServiceLocal
{
	public List<Employee> findByDepartmentAndLastName(Integer deptId, String sn);

}
