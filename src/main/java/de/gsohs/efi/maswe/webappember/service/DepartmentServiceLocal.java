package de.gsohs.efi.maswe.webappember.service;

import java.util.List;

import de.gsohs.efi.maswe.webappember.model.Department;
import javax.ejb.Local;


/**
 * service interface marked as local, meaning that this EJB will not be
 * shared across multiple Java Virtual Machines, Appservers or RichClient interactions 
 */
@Local
public interface DepartmentServiceLocal
{
	public List<Department> findAll();
	
	public Department findOne(Integer id);

}
