package de.gsohs.efi.maswe.webappember.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity implementation class for Entity: Department
 *
 */
@Entity
@Table(name="DEPARTMENTS")
@XmlRootElement(name="department")
public class Department implements Serializable {

	   
	@Id
	@Column(name="DEPARTMENT_ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DEPARTMENT_ID_GEN")
	@SequenceGenerator(name="DEPARTMENT_ID_GEN", sequenceName="DEPARTMENTS_SEQ")
	@XmlID
	private Integer id;
	@Column(name="DEPARTMENT_NAME")
	private String departmentName;
	@ManyToOne
	@JoinColumn(name="MANAGER_ID",referencedColumnName="EMPLOYEE_ID")
	private Employee manager;
	@Column(name="LOCATION_ID")
	private Integer location;
	private static final long serialVersionUID = 1L;

	public Department() {
		super();
	}   
	public Integer getDepartmentId() {
		return this.id;
	}

	public void setDepartmentId(Integer departmentId) {
		this.id = departmentId;
	}   
	public String getDepartmentName() {
		return this.departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}   
	public Employee getManager() {
		return this.manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}   
	public Integer getLocation() {
		return this.location;
	}

	public void setLocation(Integer location) {
		this.location = location;
	}
   
}
