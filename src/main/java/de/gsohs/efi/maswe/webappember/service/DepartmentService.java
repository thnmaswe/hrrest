package de.gsohs.efi.maswe.webappember.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import de.gsohs.efi.maswe.webappember.model.Department;


/**
 * Stateless Session Bean acting as Service interface for retrieving Department
 * JPA Entity instances and passing it to the calling objects
 */
@Stateless(name = "DepartmentService")
public class DepartmentService
        implements DepartmentServiceLocal
{

    /**
     * injects an instance of a Container managed JPA EntityManager
     * the mentioned persistence unit is located at src/main/resources/META-INF/persistence.xml
     */
    @PersistenceContext(unitName="hrPU")
    private EntityManager em;

    /**
     * default empty constructor
     */
    public DepartmentService() {
    }

    /**
     * queries for all department entities in database via JPQL query
     */
    @SuppressWarnings("unchecked")
	public List<Department> findAll() {
    	return (List<Department>) em.createQuery("select d from Department d").getResultList();
    }
    
    
    /**
     * retrieves a single department entity by its primary key value
     */
    @Override
	public Department findOne(Integer id) {
		return em.find(Department.class, id);
	}

}
